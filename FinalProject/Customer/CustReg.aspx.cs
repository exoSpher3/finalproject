﻿using FinalProject.Customer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FinalProject
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Add_Click(object sender, EventArgs e)
        {
            CustomerClass obj = new CustomerClass();
            obj.cust_FirstName = txt_fname.Text;
            obj.cust_LastName = txt_lname.Text;
            obj.cust_PhoneNum = txt_phone.Text;
            obj.cust_Email = txt_email.Text;
            obj.cust_Address_Num = Convert.ToInt32(txt_streetnumber.Text);
            obj.cust_Addres_St = txt_streetname.Text;
            obj.cust_City = txt_city.Text;
            obj.cust_Prov = ddl_province.SelectedItem.Text;
            obj.cust_PostalCode = txt_postalcode.Text;
            obj.cust_Pass = txt_cpassword.Text;
            List<CustomerClass> custList = CustomerAccessLayer.getAllCustomers();

            if (custList.Any<CustomerClass>(x => x.cust_Email == obj.cust_Email))
            {
                Response.Write("<script>alert('Email already exisits')</script>");
            }else
            {

                CustomerAccessLayer.addCustomer(obj);
                Response.Redirect("/Customer/cust_inventory.aspx");
            }

        }
    }
}