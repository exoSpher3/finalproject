﻿using FinalProject.Emp;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FinalProject.Customer
{
    public partial class cust_inventory : System.Web.UI.Page
    {
        //had to make a public static method to keep the lbl updated on a click of a button
        public static String displayLabel(InventoryItem obj)
        {
            return "Item Name: " + obj.part_Name + "<br/>Sold by: " + obj.part_Manufacturer
                                  + "<br/>Price: " + obj.part_Price + "<br/>Quantity: " + obj.part_Quantity
                                  + "<br/>Description: " + obj.part_Description;
        }
        int prodID;
        
        public static InventoryItem tempItem;
        public static InventoryItem item;

        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                
                btn_AddtoCart.Enabled = false;
                btnAddQuan.Enabled = false;
                btnMinQuan.Enabled = false;
            }
          
            if (Session["custID"] != null)
            {
                int custID = Convert.ToInt32(Session["custID"]);
                CustomerClass cust = new CustomerClass(CustomerAccessLayer.getCustomerFromID(custID));
                lblWelcome.Text = "Welcome to Our Inventory " + cust.cust_FirstName + " " + cust.cust_LastName;
            }else
            {
                lblWelcome.Text = "Welcome Guest";
             
            }
        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
           
            GridViewRow row = GridView1.SelectedRow;
            prodID = Convert.ToInt32(row.Cells[1].Text);
            tempItem = InventoryAccessLayer.getItem(prodID);

            if(tempItem.part_Quantity ==0)
            {
                lbl_prod.Text = "Item Out of Stock";
            }
            else
            {
                //tempItem = new InventoryItem();
                tempItem.part_Quantity = 1;

                //display to lbl the selected item information
                lbl_prod.Text = displayLabel(tempItem);

                if (Session["custID"] != null)
                {
                    btn_AddtoCart.Enabled = true;
                    btnMinQuan.Enabled = true;
                    btnAddQuan.Enabled = true;
                }
          
            }
        }

        protected void btn_AddtoCart_Click(object sender, EventArgs e)
        {
            GridViewRow row = GridView1.SelectedRow;
            prodID = Convert.ToInt32(row.Cells[1].Text);
            item = InventoryAccessLayer.getItem(prodID);
            if(item.part_Quantity >= tempItem.part_Quantity)
            {
                if(Session["cartID"] != null)
                {
                   List<InventoryItem> cartList = (List<InventoryItem>)Session["cartID"];
                   if(cartList.Any<InventoryItem>(x => x.part_ID == item.part_ID))
                    {
                        InventoryItem temp = cartList.Find(x => x.part_ID == item.part_ID);
                        cartList.Remove(temp);
                        tempItem.part_Quantity += temp.part_Quantity ;
                        cartList.Add(tempItem);
                        Session["cartID"] = cartList;
                        lblQuantity.Text = "Item Quantity added";
                    }
                    else
                    {
                        item.part_Quantity = tempItem.part_Quantity;
                        cartList.Add(tempItem);
                        Session["cartID"] = cartList;
                        lblQuantity.Text = "Item added to cart";
                    }
                }
                else
                {
                    List<InventoryItem> cartList = new List<InventoryItem>();
                    cartList.Add(tempItem);
                    lblQuantity.Text = "Item added to cart";
                    Session["cartID"] = cartList;
                }
                lblQuanError.Text = "";
            }
            else if (item.part_Quantity < tempItem.part_Quantity)
            {
                lblQuantity.Text = "";
                lblQuanError.Text = "Sorry we do not carry that many " + item.part_Name + "s";
                return;
            }
            else if (tempItem.part_Quantity == 0)
            {
                lblQuantity.Text = "";
                lblQuanError.Text = "you can't add 0 Quantity";
                return;
            }
               
            //if(Session["cartID"] != null)
            //{
            //    List<InventoryItem> cartList = (List<InventoryItem>)Session["cartID"];
            //    cartList.Add(item);
            //    Session["cartID"] = cartList;
            //    lblQuantity.Text = "Item added to cart";
            //}
            //else
            //{
            //    List<InventoryItem> cartList = new List<InventoryItem>();
            //    cartList.Add(item);
            //    lblQuantity.Text = "Item added to cart";
            //    Session["cartID"] = cartList;

            //}
            


            if (Request.Cookies["cartID"] == null)
            {
                Response.Cookies["cartID"].Value = item.part_Name + "," + item.part_Manufacturer + ","
                    + item.part_Price.ToString() + "," + item.part_Quantity.ToString() + "," + item.part_Description;
                Response.Cookies["cartID"].Expires = DateTime.Now.AddDays(1);
            }
            else
            {
           
                Response.Cookies["cartID"].Value = Request.Cookies["cartID"].Value + "|" + item.part_Name + "," + item.part_Manufacturer + ","
                    + item.part_Price.ToString() + "," + item.part_Quantity.ToString() + "," + item.part_Description;
                Response.Cookies["aa"].Expires = DateTime.Now.AddDays(1);
            }
        }

        protected void btnViewCart_Click(object sender, EventArgs e)
        {
           
            Response.Redirect("cart.aspx");
            

        }

        protected void btnAddQuan_Click(object sender, EventArgs e)
        {

            tempItem.part_Quantity += 1;
            lbl_prod.Text = displayLabel(tempItem);
        }

        protected void btnMinQuan_Click(object sender, EventArgs e)
        {
            if(tempItem.part_Quantity == 1)
            {
                lblQuanError.Text = "Can't go lower than 0 ";
            }
            else
            {
                tempItem.part_Quantity -= 1;
                lbl_prod.Text = displayLabel(tempItem);
            }
           
        }
    }

}