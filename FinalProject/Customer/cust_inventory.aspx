﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="cust_inventory.aspx.cs" Inherits="FinalProject.Customer.cust_inventory" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="login" runat="server">
            <div class="header-right">
                <asp:Label ID="lblWelcome" runat="server" Text="Label"></asp:Label>
            </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   
    <form id="form1" runat="server">

        <table style="border:solid; border-color:Highlight">
            <tr>
                <td >
                <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" BackColor="White" BorderWidth="1px" CellPadding="4" DataKeyNames="part_ID" DataSourceID="SqlDataSource1" OnSelectedIndexChanged="GridView1_SelectedIndexChanged" BorderColor="#CC9966" BorderStyle="None">
                <Columns>
                <asp:CommandField ShowSelectButton="True" buttonType="Button"/>
                <asp:BoundField DataField="part_ID" HeaderText="Part ID" InsertVisible="False" ReadOnly="True" SortExpression="part_ID" />
                <asp:BoundField DataField="part_Name" HeaderText="Part Name" SortExpression="part_Name" />
                <asp:BoundField DataField="part_Price" HeaderText="Part Price" SortExpression="part_Price" />
                <asp:BoundField DataField="part_Quantity" HeaderText="Part Quantity" SortExpression="part_Quantity" />
                <asp:BoundField DataField="part_Manufacturer" HeaderText="Part Manufacturer" SortExpression="part_Manufacturer" />
                <asp:BoundField DataField="part_Description" HeaderText="Part Description" SortExpression="part_Description" />
            </Columns>
                    <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
                    <HeaderStyle BackColor="#d70000" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />
                    <RowStyle BackColor="White" ForeColor="Black" />
                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
                    <SortedAscendingCellStyle BackColor="#FEFCEB" />
                    <SortedAscendingHeaderStyle BackColor="#AF0101" />
                    <SortedDescendingCellStyle BackColor="#F6F0C0" />
                    <SortedDescendingHeaderStyle BackColor="#7E0000" />
        </asp:GridView>
                </td>
            </tr>
            <tr>
                <td>

                <asp:Label ID="lbl_prod" runat="server" Text=""></asp:Label>

                    <br />
                    <asp:Label ID="lblQuantity" runat="server" ForeColor="#009900"></asp:Label>
                    <br />
                    <asp:Label ID="lblQuanError" runat="server" ForeColor="#CC3300"></asp:Label>

                </td> 
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnAddQuan" runat="server" OnClick="btnAddQuan_Click" Text="+" Width="52px" />
                    <asp:Button ID="btnMinQuan" runat="server" OnClick="btnMinQuan_Click" Text="-" Width="52px" />
                    <br />
                 <asp:Button ID="btn_AddtoCart" runat="server"  Text="Add to Cart" OnClick="btn_AddtoCart_Click" />
                    <asp:Button ID="btnViewCart" runat="server" OnClick="btnViewCart_Click" Text="View Cart" />
               </td>
            </tr>
        </table>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:FinalProjectDBConnectionString %>" SelectCommand="SELECT * FROM [Inventory] ORDER BY [part_Name], [part_Manufacturer], [part_Price]"></asp:SqlDataSource>
    </form>
</asp:Content>
