﻿using FinalProject.Customer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace FinalProject
{

    public class CustomerAccessLayer
    {
        public static List<CustomerClass> getAllCustomers()
        {
            List<CustomerClass> custList = new List<CustomerClass>();
            String cs = ConfigurationManager.ConnectionStrings["FinalProjectDBConnectionString"].ConnectionString;

            using (SqlConnection con = new SqlConnection(cs))
            {
                SqlCommand cmd = new SqlCommand("Select * From Customer", con);
                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while(rdr.Read())
                {
                    CustomerClass obj = new CustomerClass();
                    obj.cust_ID = Convert.ToInt32(rdr["Id"]);
                    obj.cust_FirstName = rdr["cust_First"].ToString();
                    obj.cust_LastName = rdr["cust_Last"].ToString();
                    obj.cust_PhoneNum = rdr["cust_Phone"].ToString();
                    obj.cust_Email = rdr["cust_Email"].ToString();
                    obj.cust_Address_Num = Convert.ToInt32(rdr["cust_Address_Num"]);
                    obj.cust_Addres_St = rdr["cust_Address_Name"].ToString();
                    obj.cust_City = rdr["cust_Address_City"].ToString();
                    obj.cust_Prov = rdr["cust_Address_Prov"].ToString();
                    obj.cust_PostalCode = rdr["cust_PostalCode"].ToString();
                    obj.cust_Pass = rdr["cust_Pass"].ToString();

                    custList.Add(obj);
                }

                return custList;
                
            }
        }


        public static CustomerClass customerLogin(String custEmail, String custPass)
        {
            String cs = ConfigurationManager.ConnectionStrings["FinalProjectDBConnectionString"].ConnectionString;
            using (SqlConnection con = new SqlConnection(cs))
            {
                SqlCommand cmd = new SqlCommand("Select * from Customer WHERE cust_Email=@logEmail AND cust_Pass=@logPass", con);
                con.Open();
                cmd.Parameters.AddWithValue("logEmail", custEmail);
                cmd.Parameters.AddWithValue("logPass", custPass);



                CustomerClass obj = new CustomerClass();
                SqlDataReader rdr = cmd.ExecuteReader();
                if(rdr.Read())
               {
                    obj.cust_ID = Convert.ToInt32(rdr["Id"]);
                        obj.cust_FirstName = rdr["cust_First"].ToString();
                        obj.cust_LastName = rdr["cust_Last"].ToString();
                        obj.cust_PhoneNum = rdr["cust_Phone"].ToString();
                        obj.cust_Email = rdr["cust_Email"].ToString();
                        obj.cust_Address_Num = Convert.ToInt32(rdr["cust_Address_Num"]);
                        obj.cust_Addres_St = rdr["cust_Address_Name"].ToString();
                        obj.cust_City = rdr["cust_Address_City"].ToString();
                        obj.cust_Prov = rdr["cust_Address_Prov"].ToString();
                        obj.cust_PostalCode = rdr["cust_PostalCode"].ToString();
                        obj.cust_Pass = rdr["cust_Pass"].ToString();

                        return obj;
                }
                else
                {
                    obj.cust_ID = 0;
                    return obj;
                }
            }
        }

        public static void addCustomer(CustomerClass obj)
        {
            String cs = ConfigurationManager.ConnectionStrings["FinalProjectDBConnectionString"].ConnectionString;

            using (SqlConnection con = new SqlConnection(cs))
            {
                SqlCommand cmd = new SqlCommand("insert into Customer values (@cust_First, @cust_Last, @cust_Phone, @cust_Email, @cust_AddNum, @cust_AddSt, @cust_City, @cust_Prov, @cust_POST,@cust_Pass)", con);
                con.Open();
                
                cmd.Parameters.AddWithValue("cust_First", obj.cust_FirstName);
                cmd.Parameters.AddWithValue("cust_Last", obj.cust_LastName);
                cmd.Parameters.AddWithValue("cust_Phone", obj.cust_PhoneNum);
                cmd.Parameters.AddWithValue("cust_Email", obj.cust_Email);
                cmd.Parameters.AddWithValue("cust_AddNum", obj.cust_Address_Num);
                cmd.Parameters.AddWithValue("cust_AddSt", obj.cust_Addres_St);
                cmd.Parameters.AddWithValue("cust_City", obj.cust_City);
                cmd.Parameters.AddWithValue("cust_Prov", obj.cust_Prov);
                cmd.Parameters.AddWithValue("cust_POST", obj.cust_PostalCode);
                cmd.Parameters.AddWithValue("cust_Pass", obj.cust_Pass);

                cmd.ExecuteNonQuery();
            }
        }

        public static CustomerClass getCustomerFromID(int custId)
        {
            String cs = ConfigurationManager.ConnectionStrings["FinalProjectDBConnectionString"].ConnectionString;
            using (SqlConnection con = new SqlConnection(cs))
            {
                SqlCommand cmd = new SqlCommand("Select * from Customer where Id=@custID", con);
                cmd.Parameters.AddWithValue("custID", custId);

                CustomerClass obj = new CustomerClass();
                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                if(rdr.Read())
                {
                    obj.cust_ID = Convert.ToInt32(rdr["id"]);
                    obj.cust_FirstName = rdr["cust_First"].ToString();
                    obj.cust_LastName = rdr["cust_Last"].ToString();
                    obj.cust_PhoneNum = rdr["cust_Phone"].ToString();
                    obj.cust_Email = rdr["cust_Email"].ToString();
                    obj.cust_Address_Num = Convert.ToInt32(rdr["cust_Address_Num"]);
                    obj.cust_Addres_St = rdr["cust_Address_Name"].ToString();
                    obj.cust_City = rdr["cust_Address_City"].ToString();
                    obj.cust_Prov = rdr["cust_Address_Prov"].ToString();
                    obj.cust_PostalCode = rdr["cust_PostalCode"].ToString();
                    obj.cust_Pass = rdr["cust_Pass"].ToString();
                }
                return obj;

            }
        }
    }
}