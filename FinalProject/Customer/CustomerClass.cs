﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FinalProject.Customer
{
    public class CustomerClass
    {
     
            public int cust_ID { get; set; }
            public String cust_FirstName { get; set; }
            public String cust_LastName { get; set; }
            public String cust_PhoneNum { get; set; }
            public String cust_Email { get; set; }
            public int cust_Address_Num { get; set; }
            public String cust_Addres_St { get; set; }
            public String cust_City { get; set; }
            public String cust_Prov { get; set; }
            public String cust_PostalCode { get; set; }
            public String cust_Pass { get; set; }
            public CustomerClass()
            {

            }

            public CustomerClass(CustomerClass obj)
            {
                this.cust_ID = obj.cust_ID;
                this.cust_FirstName = obj.cust_FirstName;
                this.cust_LastName = obj.cust_LastName;
                this.cust_PhoneNum = obj.cust_Email;
                this.cust_Email = obj.cust_Email;
                this.cust_Address_Num = obj.cust_Address_Num;
                this.cust_Addres_St = obj.cust_Addres_St;
                this.cust_City = obj.cust_City;
                this.cust_Prov = obj.cust_Prov;
                this.cust_PostalCode = obj.cust_PostalCode;
                this.cust_Pass = obj.cust_Pass;
            }
        }
    }
