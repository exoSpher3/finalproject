﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="cart.aspx.cs" Inherits="FinalProject.Customer.cart" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="login" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form id="form1" runat="server">
   
<%--        <asp:DataList ID="DataList1" runat="server" OnSelectedIndexChanged="DataList1_SelectedIndexChanged">
    <HeaderTemplate>
        <table>
    </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td><%#Eval("part_ID")%></td>
                <td><%#Eval("part_Name")%></td>
                <td><%#Eval("part_Manufacturer")%></td>
                <td><%#Eval("part_Price")%></td>
                <td><%#Eval("part_Quantity")%></td>
                <td><%#Eval("part_Description")%></td>

            </tr>
        </ItemTemplate>
    <FooterTemplate>
        </table>
    </FooterTemplate>
    </asp:DataList>--%>
        
        <asp:GridView ID="GridView1" runat="server" CellPadding="4" AutoGenerateColumns="False" BackColor="White" BorderColor="#CC9966" >
            <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
            <HeaderStyle BackColor="#d70000" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />
            <RowStyle BackColor="White" ForeColor="Black" />
            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
            <SortedAscendingCellStyle BackColor="#FEFCEB" />
            <SortedAscendingHeaderStyle BackColor="#AF0101" />
            <SortedDescendingCellStyle BackColor="#F6F0C0" />
            <SortedDescendingHeaderStyle BackColor="#7E0000" />
        <columns>
          <asp:boundfield datafield="part_Name" headertext="Part Name"/>
          <asp:boundfield datafield="part_Price" headertext="Part Price"/>
          <asp:boundfield datafield="part_Quantity" headertext="Part Quantity"/>
          <asp:boundfield datafield="part_Manufacturer" headertext="Part Manufacturer"/>
          <asp:boundfield datafield="part_Description" headertext="Description"/>
        </columns>
        </asp:GridView>
        <asp:Label ID="lblError" runat="server" ForeColor="#CC3300"></asp:Label>
<%--        <asp:Table ID="Table1" runat="server" BorderColor="#663300" BorderStyle="Outset" CellPadding="2" ForeColor="Black" GridLines="Both">
            <asp:TableHeaderRow>
                <asp:TableCell> ID</asp:TableCell>
                <asp:TableCell> Name</asp:TableCell>
                <asp:TableCell> Price</asp:TableCell>
                <asp:TableCell> Quantity</asp:TableCell>
                <asp:TableCell> Manufacturer</asp:TableCell>
                <asp:TableCell> Description</asp:TableCell>

            </asp:TableHeaderRow>
        </asp:Table>--%>
        <asp:Button ID="btn_CheckOut" runat="server" OnClick="btn_CheckOut_Click" Text="CheckOut" />
    </form>
    </asp:Content>
