﻿using FinalProject.Emp;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Sql;
using System.ComponentModel;
using System.Data.SqlClient;
using System.IO;

namespace FinalProject.Customer
{
    public partial class cart : System.Web.UI.Page
    {
        string s;
        //string[] a = new string[5];
        public static List<InventoryItem> tempList;
        public static BindingList<InventoryItem> bs;

        FileStream fs;
        StreamWriter fw;

        //create file stream object
        
        //create writer object
       

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int rowNum = 0;
                if (Session["cartID"] != null)
                {
                    tempList = (List<InventoryItem>)Session["cartID"];
                    GridView1.DataSource = tempList;
                    GridView1.DataBind();
                    //foreach (InventoryItem i in tempList)
                    //{
                    //    TableRow tr = new TableRow();
                    //    Table1.Rows.Add(tr);
                    //    rowNum++;
                    //    TableCell tc = new TableCell();
                    //    tc.Text = i.part_ID.ToString();
                    //    TableCell tc2 = new TableCell();
                    //    tc2.Text = i.part_Name.ToString();
                    //    TableCell tc3 = new TableCell();
                    //    tc3.Text = i.part_Price.ToString();
                    //    TableCell tc4 = new TableCell();
                    //    tc4.Text = i.part_Quantity.ToString();
                    //    TableCell tc5 = new TableCell();
                    //    tc5.Text = i.part_Manufacturer.ToString();
                    //    TableCell tc6 = new TableCell();
                    //    tc6.Text = i.part_Description.ToString();

                    //    tr.Cells.Add(tc);
                    //    tr.Cells.Add(tc2);
                    //    tr.Cells.Add(tc3);
                    //    tr.Cells.Add(tc4);
                    //    tr.Cells.Add(tc5);
                    //    tr.Cells.Add(tc6);
                    //}
                }
            }
        }

        protected void btn_CheckOut_Click(object sender, EventArgs e)
        {
            if (Session["cartID"] != null)
            {
                List<InventoryItem> cartList = (List<InventoryItem>)Session["cartID"];
                
                fs = new FileStream(@"C:\Users\win10\source\repos\finalproject2\FinalProject\Customer\receipt.txt", FileMode.Truncate , FileAccess.Write);
                fs.Flush();
                fw = new StreamWriter(fs);
                
                foreach (InventoryItem i in cartList.ToList())
                {
                    try
                    {
                        InventoryItem tempTest = InventoryAccessLayer.getItem(i.part_ID);

                        if (tempTest.part_Quantity < i.part_Quantity)
                        {
                            lblError.Text = "The Select Amount of " + i.part_Name + " is not Avaiable at the moment";
                        }
                        else if (tempTest.part_Quantity >= i.part_Quantity)
                        {
                            InventoryAccessLayer.sellItem(i.part_ID, i.part_Quantity);
                            lblError.Text = "";
                            fw.WriteLine(i.part_ID + "," + i.part_Name + "," + i.part_Price + "," + i.part_Quantity + "," + i.part_Manufacturer + "," + i.part_Description);
                            GridView1.Rows[cartList.IndexOf(i)].Visible = false;
                            cartList.RemoveAt(cartList.IndexOf(i));
                            Session["cartID"] = cartList;
                        }
                    }

                    catch (IOException) {

                    }
                }
                //remember to close
                fw.Close();
                
                fs.Close();
                Response.Redirect("../default.aspx");
            }
        }
    }
}

        /*
DataTable dt = new DataTable();
dt.Columns.AddRange(new DataColumn[5]
{
   new DataColumn("part_Name"),
   new DataColumn("part_Manufacturer"),
   new DataColumn("part_Price"),
   new DataColumn("part_Quantity"),
   new DataColumn("part_Description")

});

if (Request.Cookies["cartID"] != null)
{
   s = Convert.ToString(Request.Cookies["cartID"].Value);

   string[] itemArr = s.Split('|');

   for (int i = 0; i < itemArr.Length; i++)
   {

       string[] itemArr2 = itemArr[i].Split(',');

       for (int j = 0; j < itemArr2.Length; j++)
       {
           a[j] = itemArr2[j].ToString();
       }
       dt.Rows.Add(a[0], a[1], a[2] ,a[3], a[4]);
   }
}
DataList1.DataSource = dt;
DataList1.DataBind();
*/



    
            //GridViewRow row = GridView1.SelectedRow;
            //s = row.Cells[1].Text;
          
        

       
            //List<InventoryItem> removeItem = (List<InventoryItem>)Session["cartID"];
            //InventoryItem i = new InventoryItem();
            //i.part_Name = GridView1.SelectedRow.Cells[0].ToString();
            //removeItem.Remove(removeItem.Find(x => x.part_Name == i.part_Name));
            //GridView1.DataSource = removeItem;
            //GridView1.DataBind();

        
    
