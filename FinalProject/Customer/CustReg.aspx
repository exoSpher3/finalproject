﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CustReg.aspx.cs" Inherits="FinalProject.WebForm1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link rel="Stylesheet" href="/css/style.css" type="text/css"/>

  <form id="form1" runat="server">
        
        <div>
            <table>
                <tr>
                    <td class="auto-style2"><asp:Label ID="lbl_fname" runat="server" Text="First Name"></asp:Label></td>
                    <td class="auto-style2"><asp:TextBox ID="txt_fname" runat="server"></asp:TextBox></td>
                    <td><asp:RequiredFieldValidator ID="rqfield_fname" runat="server" ErrorMessage="First name is required!!" ControlToValidate="txt_fname"></asp:RequiredFieldValidator></td>
                    
                   
                </tr>
                <tr>
                    <td><asp:Label ID="lbl_lname" runat="server" Text="Last Name"></asp:Label></td>
                    <td><asp:TextBox ID="txt_lname" runat="server"></asp:TextBox></td>
                    <td><asp:RequiredFieldValidator ID="rqfield_lname" runat="server" ErrorMessage="Last name is required!!" ControlToValidate="txt_lname"></asp:RequiredFieldValidator></td>
                    
                </tr>
                <tr>
                    <td><asp:Label ID="lbl_phoneNumber" runat="server" Text="Phone#"></asp:Label></td>
                    <td><asp:TextBox ID="txt_phone" runat="server"></asp:TextBox></td>
                    <td><asp:RequiredFieldValidator ID="rqfield_phone" runat="server" ErrorMessage="Phone number is required!!" ControlToValidate="txt_phone"></asp:RequiredFieldValidator></td>
                    <td><asp:RegularExpressionValidator ID="regfield_phone" runat="server" ErrorMessage="Phone number must be (123)456-7890" ControlToValidate="txt_phone" ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}"></asp:RegularExpressionValidator></td>
                   
                </tr>
                 <tr>
                    <td><asp:Label ID="lbl_email" runat="server" Text="Email"></asp:Label></td>
                    <td><asp:TextBox ID="txt_email" runat="server"></asp:TextBox></td>
                    <td><asp:RequiredFieldValidator ID="rqfield_email" runat="server" ErrorMessage="Email is required!!" ControlToValidate="txt_email"></asp:RequiredFieldValidator></td>
                    <td><asp:RegularExpressionValidator ID="regfield_email" runat="server" ErrorMessage="Email must be username@domain.ext" ControlToValidate="txt_email" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator></td>
                </tr>


                 <tr>
                    <td class="auto-style2"><asp:Label ID="Label1" runat="server" Text="Street Number"></asp:Label></td>
                    <td class="auto-style2"><asp:TextBox ID="txt_streetnumber" runat="server"></asp:TextBox></td>
                    <td class="auto-style2"><asp:RequiredFieldValidator ID="rq_streetnumber" runat="server" ErrorMessage="Street number is requried" ControlToValidate="txt_streetnumber"></asp:RequiredFieldValidator></td>  
                                        <td>
                                            <asp:RangeValidator ID="rng_street" runat="server" ErrorMessage="This field has to be a number" ControlToValidate="txt_streetnumber" MaximumValue="9999" MinimumValue="0" Type="Integer"></asp:RangeValidator></td>

                 </tr>

                 <tr>
                    <td class="auto-style2"><asp:Label ID="lb_streetname" runat="server" Text="Street Name"></asp:Label></td>
                    <td class="auto-style2"><asp:TextBox ID="txt_streetname" runat="server"></asp:TextBox></td>
                    <td class="auto-style2"><asp:RequiredFieldValidator ID="rq_streetname" runat="server" ErrorMessage="Street Name is required" ControlToValidate="txt_streetname"></asp:RequiredFieldValidator></td>  
                </tr>

                 <tr>
                    <td class="auto-style2"><asp:Label ID="lb_city" runat="server" Text="City"></asp:Label></td>
                    <td class="auto-style2"><asp:TextBox ID="txt_city" runat="server"></asp:TextBox></td>
                    <td class="auto-style2"><asp:RequiredFieldValidator ID="rf_city" runat="server" ErrorMessage="City is required" ControlToValidate="txt_city"></asp:RequiredFieldValidator></td>  
                </tr>

                 <tr>
                    <td class="auto-style2"><asp:Label ID="Label4" runat="server" Text="Province"></asp:Label></td>
                    <td class="auto-style2">
                        <asp:DropDownList ID="ddl_province" runat="server">
                            <asp:ListItem>ON</asp:ListItem>
                            <asp:ListItem>QB</asp:ListItem>
                            <asp:ListItem>MB</asp:ListItem>
                            <asp:ListItem>AB</asp:ListItem>
                            <asp:ListItem>SAS</asp:ListItem>
                            <asp:ListItem>BC</asp:ListItem>
                            <asp:ListItem>NF</asp:ListItem>
                            <asp:ListItem>NB</asp:ListItem>
                            <asp:ListItem>PEI</asp:ListItem>
                            <asp:ListItem>NS</asp:ListItem>
                            <asp:ListItem>YK</asp:ListItem>
                            <asp:ListItem>NT</asp:ListItem>
                            <asp:ListItem>NU</asp:ListItem>
                        </asp:DropDownList></td>
                    <td class="auto-style2"><asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Province is required" ControlToValidate="ddl_province"></asp:RequiredFieldValidator></td>  
                </tr>

                 <tr>
                    <td class="auto-style2"><asp:Label ID="lb_postalcode" runat="server" Text="Postal Code"></asp:Label></td>
                    <td class="auto-style2"><asp:TextBox ID="txt_postalcode" runat="server"></asp:TextBox></td>
                    <td class="auto-style2"><asp:RequiredFieldValidator ID="rq_postalcode" runat="server" ErrorMessage="Postal Code is required" ControlToValidate="txt_postalcode"></asp:RequiredFieldValidator></td>
                    <td>
                        <asp:RegularExpressionValidator ID="rg_postalcode" runat="server" ErrorMessage="Postal code is LDL DLD" ControlToValidate="txt_postalcode" ValidationExpression="[A-Z][0-9][A-Z] ?[0-9][A-Z][0-9]"></asp:RegularExpressionValidator></td>
                </tr>

                 <tr>
                    <td class="auto-style2"><asp:Label ID="lb_password" runat="server" Text="Password"></asp:Label></td>
                    <td class="auto-style2"><asp:TextBox ID="txt_password" runat="server" TextMode="Password"></asp:TextBox></td>
                    <td class="auto-style2"><asp:RequiredFieldValidator ID="rq_password" runat="server" ErrorMessage="Password is required" ControlToValidate="txt_password"></asp:RequiredFieldValidator></td>  
                </tr>

                 <tr>
                    <td class="auto-style2"><asp:Label ID="lb_cpassword" runat="server" Text="Confirm Password"></asp:Label></td>
                    <td class="auto-style2"><asp:TextBox ID="txt_cpassword" runat="server" TextMode="Password"></asp:TextBox></td>
                    <td class="auto-style2"><asp:RequiredFieldValidator ID="rq_cpassword" runat="server" ErrorMessage="Please confirm your password" ControlToValidate="txt_cpassword"></asp:RequiredFieldValidator></td>  
                    <td>
                        <asp:CompareValidator ID="cm_cpassword" runat="server" ErrorMessage="Password do not match" ControlToCompare="txt_password" ControlToValidate="txt_cpassword"></asp:CompareValidator></td>
                </tr>
                <tr>
                    <td><asp:Button ID="btn_add" runat="server" OnClick="Add_Click" Text="Add" /></td>
                    
                </tr>
                
            </table>
            
        </div>
        
    </form>
</asp:Content>

<asp:Content ID="Content2" runat="server" contentplaceholderid="head">
    <style type="text/css">
        .auto-style2 {
            height: 33px;
        }
    </style>
</asp:Content>


