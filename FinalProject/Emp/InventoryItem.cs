﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FinalProject.Emp
{
    public class InventoryItem
    {
        public int part_ID { get; set; }
        public string part_Name { get; set; }
        public decimal part_Price { get; set; }
        public int part_Quantity { get; set; }
        public String part_Manufacturer { get; set; }
        public String part_Description { get; set; }
        public static object SelectedIndex { get; internal set; }

        public InventoryItem()
        {

        }

        public InventoryItem(InventoryItem obj)
        {
            this.part_ID = obj.part_ID;
            this.part_Name = obj.part_Name;
            this.part_Price = obj.part_Price;
            this.part_Quantity = obj.part_Quantity;
            this.part_Manufacturer = obj.part_Manufacturer;
            this.part_Description = obj.part_Description;
        }

        internal static void DeleteRow(object selectedIndex)
        {
            throw new NotImplementedException();
        }
    }
}