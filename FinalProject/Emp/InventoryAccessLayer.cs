﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace FinalProject.Emp
{

    public class InventoryAccessLayer
    {
        public List<InventoryItem> getAllItems()
        {
           
            List<InventoryItem> listOfItems = new List<InventoryItem>();
            String cs = ConfigurationManager.ConnectionStrings["FinalProjectDBConnectionString"].ConnectionString;

            using (SqlConnection con = new SqlConnection(cs))
            {
                SqlCommand cmd = new SqlCommand("Select * From Inventory", con);
                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while(rdr.Read())
                {
                    InventoryItem obj = new InventoryItem();
                    obj.part_ID = Convert.ToInt32(rdr["part_ID"]);
                    obj.part_Name = rdr["part_Name"].ToString();
                    obj.part_Price = Convert.ToDecimal(rdr["part_Price"]);
                    obj.part_Quantity = Convert.ToInt32(rdr["part_Quantity"]);
                    obj.part_Manufacturer = rdr["part_Manufacturer"].ToString();
                    obj.part_Description = rdr["part_Description"].ToString();

                    listOfItems.Add(obj);
                }

                return listOfItems;
            }
        }

        public static void addItem(InventoryItem obj)
        {
            String cs = ConfigurationManager.ConnectionStrings["FinalProjectDBConnectionString"].ConnectionString;

            using (SqlConnection con = new SqlConnection(cs))
            {
                SqlCommand cmd = new SqlCommand("Insert into Inventory values (@partName, @partPrice, @partQuan, @partMan, @partDes)", con);
                con.Open();
                cmd.Parameters.AddWithValue("partName", obj.part_Name);
                cmd.Parameters.AddWithValue("partPrice", obj.part_Price);
                cmd.Parameters.AddWithValue("partQuan", obj.part_Quantity);
                cmd.Parameters.AddWithValue("partMan", obj.part_Manufacturer);
                cmd.Parameters.AddWithValue("partDes", obj.part_Description);

                cmd.ExecuteNonQuery();
            }
        }

        public static void deleteItem(InventoryItem obj)
        {
            String cs = ConfigurationManager.ConnectionStrings["FinalProjectDBConnectionString"].ConnectionString;

            using (SqlConnection con = new SqlConnection(cs))
            {
                SqlCommand cmd = new SqlCommand("delete from inventory WHERE part_ID=@partID",con);
                con.Open();
                cmd.Parameters.AddWithValue("partID",obj.part_ID);
                cmd.ExecuteNonQuery();
            }
        }

        public static void updateItem(InventoryItem obj)
        {
            String cs = ConfigurationManager.ConnectionStrings["FinalProjectDBConnectionString"].ConnectionString;

            using (SqlConnection con = new SqlConnection(cs))
            {
                SqlCommand cmd = new SqlCommand("Update Inventory set part_Name=@pName, part_Price=@pPrice, part_Quantity=@pQuan, part_Manufacturer=@pMan, part_Description=@pDes where part_Id=@pID", con);
                con.Open();
                cmd.Parameters.AddWithValue("pName", obj.part_Name);
                cmd.Parameters.AddWithValue("pPrice", obj.part_Price);
                cmd.Parameters.AddWithValue("pQuan", obj.part_Quantity);
                cmd.Parameters.AddWithValue("pMan", obj.part_Manufacturer);
                cmd.Parameters.AddWithValue("pDes", obj.part_Description);
                cmd.Parameters.AddWithValue("pID", obj.part_ID);

                cmd.ExecuteNonQuery();

            }
        }

        public static InventoryItem getItem(int itemID)
        {
            String cs = ConfigurationManager.ConnectionStrings["FinalProjectDBConnectionString"].ConnectionString;

            using (SqlConnection con = new SqlConnection(cs))
            {
                SqlCommand cmd = new SqlCommand("Select * From Inventory where part_ID=@itemID", con);
                cmd.Parameters.AddWithValue("itemID", itemID);
                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                InventoryItem obj = new InventoryItem();
                if (rdr.Read())
                {
                   
                    obj.part_ID = Convert.ToInt32(rdr["part_ID"]);
                    obj.part_Name = rdr["part_Name"].ToString();
                    obj.part_Price = Convert.ToDecimal(rdr["part_Price"]);
                    obj.part_Quantity = Convert.ToInt32(rdr["part_Quantity"]);
                    obj.part_Manufacturer = rdr["part_Manufacturer"].ToString();
                    obj.part_Description = rdr["part_Description"].ToString();
                }
                return obj;
            }
        }

        public static void sellItem(int prodId, int amt)
        {
            String cs = ConfigurationManager.ConnectionStrings["FinalProjectDBConnectionString"].ConnectionString;

            using (SqlConnection con = new SqlConnection(cs))
            {
                SqlCommand cmd = new SqlCommand("Update Inventory set part_Quantity=part_Quantity-@amountSold where part_Id=@pID", con);
                con.Open();
                cmd.Parameters.AddWithValue("pID", prodId);
                cmd.Parameters.AddWithValue("amountSold", amt);
                cmd.ExecuteNonQuery();
            }
        }
    }


}   