﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="inventory.aspx.cs" Inherits="FinalProject.inventory" %>

    
<asp:Content ID="ContentPlaceHolder1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <link rel="Stylesheet" href="/css/style.css" type="text/css"/>
    <form id="inventoryList" runat="server">
  
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:FinalProjectDBConnectionString2 %>" SelectCommand="SELECT * FROM [Inventory] ORDER BY [part_ID]"></asp:SqlDataSource>
        <asp:ListView ID="ListView1" runat="server" DataKeyNames="part_ID" DataSourceID="ObjectDataSource1" InsertItemPosition="LastItem">
            <AlternatingItemTemplate>
                <td runat="server" style="background-color:#FFF8DC;">part_ID:
                    <asp:Label ID="part_IDLabel" runat="server" Text='<%# Eval("part_ID") %>' />
                    <br />
                    part_Name:
                    <asp:Label ID="part_NameLabel" runat="server" Text='<%# Eval("part_Name") %>' />
                    <br />
                    part_Price:
                    <asp:Label ID="part_PriceLabel" runat="server" Text='<%# Eval("part_Price") %>' />
                    <br />
                    part_Quantity:
                    <asp:Label ID="part_QuantityLabel" runat="server" Text='<%# Eval("part_Quantity") %>' />
                    <br />
                    part_Manufacturer:
                    <asp:Label ID="part_ManufacturerLabel" runat="server" Text='<%# Eval("part_Manufacturer") %>' />
                    <br />
                    part_Description:
                    <asp:Label ID="part_DescriptionLabel" runat="server" Text='<%# Eval("part_Description") %>' />
                    <br />
                    <asp:Button ID="EditButton" runat="server" CommandName="Edit" Text="Edit" />
                    <asp:Button ID="DeleteButton" runat="server" CommandName="Delete" Text="Delete" />
                </td>
            </AlternatingItemTemplate>
            <EditItemTemplate>
                <td runat="server" style="background-color:#008A8C;color: #FFFFFF;">part_ID:
                    <asp:TextBox ID="part_IDTextBox" runat="server" Text='<%# Bind("part_ID") %>' />
                    <br />
                    part_Name:
                    <asp:TextBox ID="part_NameTextBox" runat="server" Text='<%# Bind("part_Name") %>' />
                    <br />
                    part_Price:
                    <asp:TextBox ID="part_PriceTextBox" runat="server" Text='<%# Bind("part_Price") %>' />
                    <br />
                    part_Quantity:
                    <asp:TextBox ID="part_QuantityTextBox" runat="server" Text='<%# Bind("part_Quantity") %>' />
                    <br />
                    part_Manufacturer:
                    <asp:TextBox ID="part_ManufacturerTextBox" runat="server" Text='<%# Bind("part_Manufacturer") %>' />
                    <br />
                    part_Description:
                    <asp:TextBox ID="part_DescriptionTextBox" runat="server" Text='<%# Bind("part_Description") %>' />
                    <br />
                    <asp:Button ID="UpdateButton" runat="server" CommandName="Update" Text="Update" />
                    <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" Text="Cancel" />
                </td>
            </EditItemTemplate>
            <EmptyDataTemplate>
                <table style="background-color: #FFFFFF;border-collapse: collapse;border-color: #999999;border-style:none;border-width:1px;font-family: Verdana, Arial, Helvetica, sans-serif;">
                    <tr>
                        <td>No data was returned.</td>
                    </tr>
                </table>
            </EmptyDataTemplate>
            <InsertItemTemplate>
                <td runat="server" style="">part_ID:
                    <asp:TextBox ID="part_IDTextBox" runat="server" Text='<%# Bind("part_ID") %>' />
                    <br />
                    part_Name:
                    <asp:TextBox ID="part_NameTextBox" runat="server" Text='<%# Bind("part_Name") %>' />
                    <br />
                    part_Price:
                    <asp:TextBox ID="part_PriceTextBox" runat="server" Text='<%# Bind("part_Price") %>' />
                    <br />
                    part_Quantity:
                    <asp:TextBox ID="part_QuantityTextBox" runat="server" Text='<%# Bind("part_Quantity") %>' />
                    <br />
                    part_Manufacturer:
                    <asp:TextBox ID="part_ManufacturerTextBox" runat="server" Text='<%# Bind("part_Manufacturer") %>' />
                    <br />
                    part_Description:
                    <asp:TextBox ID="part_DescriptionTextBox" runat="server" Text='<%# Bind("part_Description") %>' />
                    <br />
                    <asp:Button ID="InsertButton" runat="server" CommandName="Insert" Text="Insert" />
                    <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" Text="Clear" />
                </td>
            </InsertItemTemplate>
            <ItemTemplate>
                <td runat="server" style="background-color:#DCDCDC;color: #000000;">part_ID:
                    <asp:Label ID="part_IDLabel" runat="server" Text='<%# Eval("part_ID") %>' />
                    <br />
                    part_Name:
                    <asp:Label ID="part_NameLabel" runat="server" Text='<%# Eval("part_Name") %>' />
                    <br />
                    part_Price:
                    <asp:Label ID="part_PriceLabel" runat="server" Text='<%# Eval("part_Price") %>' />
                    <br />
                    part_Quantity:
                    <asp:Label ID="part_QuantityLabel" runat="server" Text='<%# Eval("part_Quantity") %>' />
                    <br />
                    part_Manufacturer:
                    <asp:Label ID="part_ManufacturerLabel" runat="server" Text='<%# Eval("part_Manufacturer") %>' />
                    <br />
                    part_Description:
                    <asp:Label ID="part_DescriptionLabel" runat="server" Text='<%# Eval("part_Description") %>' />
                    <br />
                    <asp:Button ID="EditButton" runat="server" CommandName="Edit" Text="Edit" />
                    <asp:Button ID="DeleteButton" runat="server" CommandName="Delete" Text="Delete" />
                </td>
            </ItemTemplate>
            <LayoutTemplate>
                <table runat="server" border="1" style="background-color: #FFFFFF;border-collapse: collapse;border-color: #999999;border-style:none;border-width:1px;font-family: Verdana, Arial, Helvetica, sans-serif;">
                    <tr id="itemPlaceholderContainer" runat="server">
                        <td id="itemPlaceholder" runat="server"></td>
                    </tr>
                </table>
                <div style="text-align: center;background-color: #CCCCCC;font-family: Verdana, Arial, Helvetica, sans-serif;color: #000000;">
                    <asp:DataPager ID="DataPager1" runat="server">
                        <Fields>
                            <asp:NextPreviousPagerField ButtonType="Button" ShowFirstPageButton="True" ShowLastPageButton="True" />
                        </Fields>
                    </asp:DataPager>
                </div>
            </LayoutTemplate>
            <SelectedItemTemplate>
                <td runat="server" style="background-color:#008A8C;font-weight: bold;color: #FFFFFF;">part_ID:
                    <asp:Label ID="part_IDLabel" runat="server" Text='<%# Eval("part_ID") %>' />
                    <br />
                    part_Name:
                    <asp:Label ID="part_NameLabel" runat="server" Text='<%# Eval("part_Name") %>' />
                    <br />
                    part_Price:
                    <asp:Label ID="part_PriceLabel" runat="server" Text='<%# Eval("part_Price") %>' />
                    <br />
                    part_Quantity:
                    <asp:Label ID="part_QuantityLabel" runat="server" Text='<%# Eval("part_Quantity") %>' />
                    <br />
                    part_Manufacturer:
                    <asp:Label ID="part_ManufacturerLabel" runat="server" Text='<%# Eval("part_Manufacturer") %>' />
                    <br />
                    part_Description:
                    <asp:Label ID="part_DescriptionLabel" runat="server" Text='<%# Eval("part_Description") %>' />
                    <br />
                    <asp:Button ID="EditButton" runat="server" CommandName="Edit" Text="Edit" />
                    <asp:Button ID="DeleteButton" runat="server" CommandName="Delete" Text="Delete" />
                </td>
            </SelectedItemTemplate>
        </asp:ListView>
        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" DataObjectTypeName="FinalProject.Emp.InventoryItem" DeleteMethod="deleteItem" InsertMethod="addItem" SelectMethod="getAllItems" TypeName="FinalProject.Emp.InventoryAccessLayer" UpdateMethod="updateItem"></asp:ObjectDataSource>
        </form>
  
    </asp:Content>
