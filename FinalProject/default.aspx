﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="FinalProject._default" %>

<asp:Content ID="login" ContentPlaceHolderID="login" runat="server">
    <link rel="Stylesheet" href="/css/style.css" type="text/css"/>

    <div class="header-right">
        <form id="loginForm" runat="server">
            <asp:Label ID="lblUser" runat="server" Text="Username"></asp:Label>     
            <asp:TextBox ID="txtUser" runat="server"></asp:TextBox>
            <br />
            <asp:RegularExpressionValidator ID="userNameValidation" runat="server" ControlToValidate="txtUser" Display="Dynamic" ErrorMessage="Email is formated in correctly" ForeColor="#CC3300" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
            <br />
            <asp:Label ID="lblPass" runat="server" Text="Password"></asp:Label>
            <asp:TextBox ID="txtPass" runat="server" TextMode="Password"></asp:TextBox>
            <br />
            <br /><asp:Button ID="btnLogin" runat="server" Text="Login" OnClick="btnLogin_Click" /> &nbsp; &nbsp;
            <asp:Button ID="btnSignUp" runat="server" Text="Sign Up" OnClick="btnSignUp_Click" />
            
        </form>
        
    </div>
    </asp:Content>     

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <link rel="Stylesheet" href="/css/style.css" type="text/css"/>
    <p>
        Welcome to Sheridan Computing Co. We are an online Canadian electronic retailer dealing in 
        electronics, computers, and computer components for business and corporate customers. 
        We have the best deals in town and unbeatable prices, to make a purchase use the "Sign Up" button above.
        You will be able to browse through the inventory as a Guest, but if any purchases are being made please register
        yourself using the "Sign Up" button above.
        
    </p>
    
</asp:Content>

