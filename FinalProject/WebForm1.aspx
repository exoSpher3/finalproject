﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="FinalProject.WebForm1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link rel="Stylesheet" href="/css/style.css" type="text/css"/>

<form id="form1" runat="server">
        
        <div>
            <table>
                <tr>
                    <td><asp:Label ID="lbl_fname" runat="server" Text="First Name"></asp:Label></td>
                    <td><asp:TextBox ID="txt_fname" runat="server"></asp:TextBox></td>
                    
                </tr>
                <tr>
                    <td><asp:Label ID="lbl_lname" runat="server" Text="Last Name"></asp:Label></td>
                    <td><asp:TextBox ID="txt_lname" runat="server"></asp:TextBox></td>
                    
                </tr>
                

                <tr>
                    <td><asp:Button ID="btn_add" runat="server" OnClick="Add_Click" Text="Add" /></td>
                    
                </tr>
                
            </table>
            
        </div>
        
    </form>
</asp:Content>

