﻿using FinalProject.Customer;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FinalProject
{
    public partial class _default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(Session["custID"] != null)
            {
                int custID = Convert.ToInt32(Session["custID"]);
                CustomerClass cust = new CustomerClass(CustomerAccessLayer.getCustomerFromID(custID));
                loginForm.InnerHtml = "<p style='float:left;'> Welcome to our Store " + cust.cust_FirstName + " " + cust.cust_LastName + "</p>";
            }
        }

        protected void btnSignUp_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Customer/CustReg.aspx");
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            CustomerClass loginTest = new CustomerClass(CustomerAccessLayer.customerLogin(txtUser.Text, txtPass.Text));

            if (txtUser.Text == "admin@admin.com" && txtPass.Text == "admin")
            {
                Response.Redirect("/Emp/Inventory.aspx");

            }
            else if (loginTest.cust_ID > 0)
            {
             
                Session["CustID"] = loginTest.cust_ID;
                Response.Redirect("/Customer/cust_Inventory.aspx");
            }
            else
            {
                Response.Write("<script> alert('Log in error please check Email and Password')</script>");
            }
        }
    }
}